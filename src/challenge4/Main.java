package challenge4;

//mengubah objek hewan menjadi bertipe data Anjing menggunakan type casting
public class Main {
   public static void main(String[] args) {
      Hewan hewan = new Anjing();
      ((Anjing)hewan).suara();
   }
}

//Type casting variabel hewan menjadi variabel Anjing